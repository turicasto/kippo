from kippo.core import dblog
from twisted.enterprise import adbapi
from twisted.internet import defer
from twisted.python import log
from elasticsearch import Elasticsearch
import GeoIP
import Geohash
import time
import uuid

GEO = GeoIP.open("/usr/share/GeoIP/GeoIPCity.dat", GeoIP.GEOIP_STANDARD)

class ElasticConnector(object):
    def __init__(self, host, port, index, docType):
        self.host = host
        self.port = port
        self.index = index
        self.docType = docType
        self.es = Elasticsearch([
            self.host + ':' + str(self.port)
        ], use_ssl = True)
        self.es.index(self.index, self.docType, {})
        self.geomap = {self.docType : { "properties" : { "geohash" : { "type" : "geo_point" } } } }
        self.es.indices.put_mapping(self.docType, self.geomap, index = self.index)
        self.geomap = {self.docType : { "properties" : {'content': { 'type' : 'string', 'index' : 'not_analyzed' } } } }
        self.es.indices.put_mapping(self.docType, self.geomap, index = self.index)
        self.geomap = {self.docType : { "properties" : {'country': { 'type' : 'string', 'index' : 'not_analyzed' } } } }
        self.es.indices.put_mapping(self.docType, self.geomap, index = self.index)
        self.geomap = {self.docType : { "properties" : { 'city' : { 'type' : 'string', 'index' : 'not_analyzed' } } } }
        self.es.indices.put_mapping(self.docType, self.geomap, index = self.index)
        self.geomap = {self.docType : { "properties" : { 'date' : { 'type' : 'date', 'index' : 'not_analyzed', 'format': 'yyyy-MM-dd HH:mm:ss' } } } }
        self.es.indices.put_mapping(self.docType, self.geomap, index = self.index)

    def sendData(self, data):
        print self.index
        print self.docType
        print data
        self.es.index(self.index, self.docType, data)

class ElasticLog(object):

    def __init__(self, id, date, content, ip, port):
        self.id = id
        self.date = date
        self.content = content
        self.ip = ip
        self.port = port
        self.record = GEO.record_by_addr(self.ip)
        if self.record is None:
            self.ip = "127.0.0.1"
            self.city = "debug"
            self.country = "debug"
            self.latitude = 39.3317
            self.longitude = 16.1839
            self.geohash = Geohash.encode(self.latitude, self.longitude)
        else:
            self.geohash = Geohash.encode(self.record["latitude"], self.record["longitude"])
            self.city = self.record["city"]
            self.country = self.record["country_name"]
            self.latitude = self.record["latitude"]
            self.longitude = self.record["longitude"]

    def getLog(self):
        return {'session_id' : self.id,
                'date' : self.date,
                'content' : self.content,
                'ip' : self.ip,
                'port'	    : self.port,
                'geohash'  : self.geohash,
                'city'     : self.city,
                'country'  : self.country,
                'latitude' : self.latitude,
                'longitude': self.longitude,
        }

class DBLogger(dblog.DBLogger):
    def start(self, cfg):
        if cfg.has_option('database_elastic', 'port'):
            port = int(cfg.get('database_elastic', 'port'))
        else:
            port = 9200
        
        host = cfg.get('database_elastic', 'host')
        index = cfg.get('database_elastic', 'index')
        docType = cfg.get('database_elastic', 'document')
        
        self.esConnector = ElasticConnector(host, port, index, docType)
        self.sessionTable = {}

    def write(self, logObj):
        logObj.date = time.strftime('%Y-%m-%d %H:%M:%S')
        self.esConnector.sendData(logObj.getLog())

    def createSession(self, peerIP, peerPort, hostIP, hostPort):
        sid = uuid.uuid1().hex
        sensorname = self.getSensor() or hostIP
        date_field = time.strftime('%Y-%m-%d %H:%M:%S')
        logObj = ElasticLog(sid, date_field,
                            "New connection: %s:%s" % (peerIP, peerPort),
                            peerIP, peerPort)
        self.esConnector.sendData(logObj.getLog())
        self.sessionTable[sid] = logObj
        return sid

    def handleConnectionLost(self, session, args):
        logObj = self.sessionTable[session]
        logObj.content = 'Connection lost'
        self.write(logObj)
        self.sessionTable[session] = None

    def handleLoginFailed(self, session, args):
        logObj = self.sessionTable[session]
        logObj.content =  'Login failed [%s/%s]' % \
            (args['username'], args['password'])
        self.write(logObj)

    def handleLoginSucceeded(self, session, args):
        logObj = self.sessionTable[session]
        logObj.content =  'Login succeeded [%s/%s]' % \
            (args['username'], args['password'])
        self.write(logObj)

    def handleCommand(self, session, args):
        logObj = self.sessionTable[session]
        logObj.content = 'Command [%s]' % (args['input'],)
        self.write(logObj)

    def handleUnknownCommand(self, session, args):
        logObj = self.sessionTable[session]
        logObj.content = 'Unknown command [%s]' % (args['input'],)
        self.write(logObj)

    def handleInput(self, session, args):
        logObj = self.sessionTable[session]
        logObj.content = 'Input [%s] @%s' % (args['input'], args['realm'])
        self.write(logObj)

    def handleTerminalSize(self, session, args):
        logObj = self.sessionTable[session]
        logObj.content = 'Terminal size: %sx%s' % \
            (args['width'], args['height'])
        self.write(logObj)

    def handleClientVersion(self, session, args):
        logObj = self.sessionTable[session]
        logObj.content = 'Client version: [%s]' % (args['version'],)
        self.write(logObj)

    def handleFileDownload(self, session, args):
        logObj = self.sessionTable[session]
        logObj.content = 'File download: [%s] -> %s' % \
            (args['url'], args['outfile'])
        self.write(logObj)